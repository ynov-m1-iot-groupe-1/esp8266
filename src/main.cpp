#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Card.h>
#include <MQTTService.h>
#include "DHT.h"
#include <ArduinoJson.h>
 

#define DHTTYPE DHT11
#define DHTPIN  5 //Ici faut mettre le port des data. Le 5 correspond au port D1.
#define SOUNDPIN  4 
/* Pour info :
D0   = 16;
D1   = 5;
D2   = 4;
D3   = 0;
D4   = 2;
D5   = 14;
D6   = 12;
D7   = 13;
D8   = 15;
D9   = 3;
D10  = 1;
*/
WiFiClient espClient;
PubSubClient client(espClient);
const char *GROUPE1 ="YNOV/GAL"; 
const char *TEMPERATURE = "YNOV/DHT11/GAL/TEMP";
const char *HUMIDITY = "YNOV/DHT11/GAL/HUM";
const char *GROUNDHUMIDITY = "YNOV/SENSOIL_SENSOR/GAL/POURCENTAGE";
const char *LED = "YNOV/LED/GAL/BOOL";

bool _isFirstMessage = true;
float _temp;
float _hum;
float _grHum;
int _led;

DHT dht(DHTPIN, DHTTYPE);

using namespace CardMethods;
using namespace MQTTService;
void setup() {
  Serial.begin(115200);
  Setup_Wifi();
  SetClient(client);  
  dht.begin();
  SetLED();
  SetGroundHumidity();
}

void loop() {
  if (!client.connected()) {
    Reconnect(client, GROUPE1);
  }
  client.loop();

  if(_isFirstMessage)  MQTTService::Publish(client, GROUPE1, "Connexion de la carte sur le topic général");
  _isFirstMessage=false;

  float temp= GetTemperature(dht);
  float hum = GetHumidity(dht);
  float grHum = GetGroundHumidity();
  int led = GetStateLED();
  
  if(temp != _temp){
    Publish(client, TEMPERATURE, temp);
    _temp=temp;
  }
   if(hum != _hum){
    Publish(client, HUMIDITY, hum);
    _hum=hum;
  }
  if(grHum != _grHum){
    Publish(client, GROUNDHUMIDITY, grHum);
    _grHum=grHum;
  }
  if(led!=_led){
    Publish(client, LED, led);
    _led=led;
  }
  delay(5000);
}


