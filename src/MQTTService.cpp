
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "MQTTService.h"
#include <string>
#include <cstring>
#include <Card.h>

#define MSG_BUFFER_SIZE (50)
char msg[MSG_BUFFER_SIZE];
const char *LEDLISTENING = "YNOV/LED/GAL/BOOL/LISTENING";

using namespace MQTTService;
using namespace std;

namespace MQTTService
{
    // const char *mqtt_server = "broker.mqtt-dashboard.com";
    const char *mqtt_server = ""; // Broker address
    const int port = 1883;
    void SetClient(PubSubClient &client)
    {
        client.setServer(mqtt_server, port);
        client.setCallback(Callback);
    }

    void Reconnect(PubSubClient &client, const char *topic)
    {
        // Loop until we're reconnected
        while (!client.connected())
        {
            Serial.print("Attempting MQTT connection...");
            // Create a random client ID
            String clientId = "ESP8266Client-";
            clientId += String(random(0xffff), HEX);
            // Attempt to connect
            if (client.connect(clientId.c_str()))
            {
                Serial.println("connected");
                // Once connected, publish an announcement...
                client.publish(topic, "Reconnection to the topic done");
                // ... and resubscribe
                client.subscribe(LEDLISTENING);
            }
            else
            {
                Serial.print("failed, rc=");
                Serial.print(client.state());
                Serial.println(" try again in 5 seconds");
                // Wait 5 seconds before retrying
                delay(5000);
            }
        }
    }

    void Callback(char *topic, byte *payload, unsigned int length)
    {
        Serial.print("Message arrived on [");
        Serial.print(topic);
        Serial.println("] ");
        for (int i = 0; i < length; i++)
        {
            Serial.print((char)payload[i]);
        }

        if ((char)payload[0] == '1')
            CardMethods::SetStateLED(true);
        else
            CardMethods::SetStateLED(false);
    }

    void Publish(PubSubClient &client, const char *topic, const char *message)
    {
        snprintf(msg, MSG_BUFFER_SIZE, message);
        client.publish(topic, msg);
        Serial.print("------ Publish message on : ");
        Serial.print(topic);
        Serial.println("------");
        Serial.println(msg);
        Serial.println("--------------------------------------");

        // char buffer[256];
        // serializeJson("{\"temp\": 2.33}", buffer);
        // client.publish("ynov-iot-m1-groupe-1/card/temp", buffer);
    }

    void Publish(PubSubClient &client, const char *topic, float value)
    {
        Publish(client, topic, dtostrf(value, 4, 3, msg));
    }
} // namespace MQTTService
