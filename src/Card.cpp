#include "Card.h"
#include <ESP8266WiFi.h>
#include "DHT.h"

using namespace CardMethods;

#define SOIL_ANALOG_PIN A0
#define LED LED_BUILTIN

namespace CardMethods
{

    void Setup_Wifi()
    {
        const char *ssid = "----";     // Wifi name
        const char *password = "----"; // Wifi password

        delay(10);
        // We start by connecting to a WiFi network
        Serial.println();
        Serial.print("Connecting to ");
        Serial.println(ssid);

        WiFi.mode(WIFI_STA);
        WiFi.begin(ssid, password);

        while (WiFi.status() != WL_CONNECTED)
        {
            delay(500);
            Serial.print(".");
        }

        randomSeed(micros());

        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
    }
    float GetTemperature(DHT &dht)
    {
        float temp = dht.readTemperature();
        Serial.print("Temperature = ");
        Serial.print(temp);
        Serial.println("°");
        return temp;
    }
     float GetHumidity(DHT &dht)
    {
        float hum = dht.readHumidity();
        Serial.print("Humidite = ");
        Serial.print(hum);
        return hum;
    }
    void SetGroundHumidity()
    {
        pinMode(SOIL_ANALOG_PIN, INPUT);
        digitalWrite(LED, HIGH);
    }
    float GetGroundHumidity()
    {
        int humidity = analogRead(SOIL_ANALOG_PIN);
        humidity = map(humidity, 1024, 360, 0, 100);
        Serial.print("Humidité : ");
        Serial.print(humidity);
        Serial.println("%");
        return humidity;
    }

    void SetLED()
    {
        pinMode(LED, OUTPUT);
    }

    int GetStateLED()
    {
        if (digitalRead(LED) == 1)
            return 0;
        else
            return 1;
    }

    void SetStateLED(bool isLight)
    {
        if (isLight)
            digitalWrite(LED, LOW);
        else
            digitalWrite(LED, HIGH);
    }
} // namespace CardMethods