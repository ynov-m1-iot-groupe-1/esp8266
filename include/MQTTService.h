#include <PubSubClient.h>

namespace MQTTService {
   void Reconnect(PubSubClient& client,const char *topic);
   void SetClient(PubSubClient& client);
   void Callback(char* topic, byte* payload, unsigned int length);
   void Publish(PubSubClient& client, const char *topic, const char *message);
   void Publish(PubSubClient& client, const char *topic, float value);

}