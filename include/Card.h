#include "DHT.h"

namespace CardMethods
{
    void Setup_Wifi();  
    float GetTemperature(DHT& dht);
    float GetHumidity(DHT& dht);
    float GetGroundHumidity();
    void SetGroundHumidity();
    void SetLED();
    int GetStateLED();
    void SetStateLED(bool isLight);
} // namespace CardMethods